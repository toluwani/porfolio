/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 *
 * @author DR BOLADALE ALONGE
 */
@WebServlet(name = "logincontroller", urlPatterns = {"/logincontroller"})
public class logincontroller extends HttpServlet {

    public static boolean isLoggedIn = false;
    public static String name = "", email = "";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            /* TODO output your page here. You may use following sample code. */

            email = request.getParameter("email");
            name = request.getParameter("name");

            if (email != null) {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cv", "root", "");
                String Query = "select * from personal_details  where email=? and name=? ";
                PreparedStatement psm = conn.prepareStatement(Query);
                psm.setString(1, email);
                psm.setString(2, name);
                ResultSet rs = psm.executeQuery();

                if ((isLoggedIn = rs.next())) {
                    if(request.getParameter("mobile") != null){
                        response.getWriter().print("success");
                    }else{
                        request.getRequestDispatcher("index.jsp").forward(request, response);
                    }
                }else{
                    email = "";
                    name = "";
                    request.setAttribute("error", "Invalid name or email");
                    if(request.getParameter("mobile") != null){
                        response.getWriter().print("failure");
                    }else{
                        request.getRequestDispatcher("login.jsp").forward(request, response);
                    }
                }

            }

        } catch (Exception ex) {

            ex.printStackTrace();

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
