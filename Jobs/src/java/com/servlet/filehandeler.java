/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servlet;

import java.io.File;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import javax.swing.filechooser.FileSystemView;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author DR BOLADALE ALONGE
 */
@WebServlet(name = "filehandeler", urlPatterns = {"/filehandeler"})

public class filehandeler extends HttpServlet {

    static final FileSystemView FSV = FileSystemView.getFileSystemView();
    static final File FILE = new File((FSV.getHomeDirectory().getPath()+"\\.uploads\\"));
    static{
        FILE.mkdir();
    }
    private final String UPLOAD_DIRECTORY = FILE.getAbsolutePath();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println(request.getAttribute("message"));
            out.println(request.getParameter("id"));
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //process only if its multipart content
        if (ServletFileUpload.isMultipartContent(request)) {
            String path, name;

            try {
                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

                for (FileItem item : multiparts) {

                    if (!item.isFormField()) {
                        name = new File(item.getName()).getName();
                        item.write(new File((path = UPLOAD_DIRECTORY + File.separator + name)));
                        // save the path to the db
                        Class.forName("com.mysql.jdbc.Driver");
                        try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/cv", "root", "") // stmt.execute("insert into `phototable`.`albumcolumn`
                                ) {
                            conn.createStatement().execute("insert into uploads values('" + logincontroller.email + "','" + path + "' )");
                        }
                    }

                }

                //File uploaded successfully
                request.setAttribute("message", "File Uploaded Successfully");

            } catch (Exception ex) {
//
                request.setAttribute("message", "File Upload Failed due to " + ex.getLocalizedMessage());
//
            }

        } else {

            request.setAttribute("message", "Sorry this Servlet only handles file upload request");

        }

        processRequest(request, response);

//        request.getRequestDispatcher("/result.jsp").forward(request, response);
    }

}
