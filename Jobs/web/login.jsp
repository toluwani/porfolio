<%-- 
    Document   : login
    Created on : Nov 28, 2017, 4:17:56 PM
    Author     : DR BOLADALE ALONGE
--%>

<%@page import="com.servlet.logincontroller"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% if(request.getParameter("x") != null){
    logincontroller.isLoggedIn = false;
    logincontroller.name = "";
}%>

<html>

        <head>
            <meta charset="utf-8">

            <title>Pivot | Login</title>
            <meta name="description" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <link href="css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/theme.css" rel="stylesheet" type="text/css" media="all"/>
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
            <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        </head>
        <body>
            <div class="loader hidden">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>

            <div class="nav-container">
                <nav class="top-bar overlay-bar">
                    <div class="container">


                        <div class="row nav-menu">
                            <div class="col-sm-3 col-md-2 columns">
                                <a href="index.html">

                                </a>
                            </div>

                            <div class="col-sm-9 col-md-10 columns">
                                <ul class="menu">




                                    <li class=""><a href="index.jsp">home</a>

                                    </li>
                                    <li class=""><a href="https://www.upwork.com/i/how-it-works/client/">findjobs</a>

                                    </li>



                                </ul>

                                <ul class="social-icons text-right">
                                    <li>
                                        <a href="#">
                                            <i class="icon social_twitter"></i>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#">
                                            <i class="icon social_facebook"></i>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#">
                                            <i class="icon social_instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div><!--end of row-->

                        <div class="mobile-toggle">
                            <i class="icon icon_menu"></i>
                        </div>

                    </div><!--end of container-->
                </nav>
            </div>
            <div class="main-container">
                <section class="no-pad login-page fullscreen-element">

                    <div class="background-image-holder">
                        <img class="background-image" alt="Poster Image For Mobiles" src="img/hero6.jpg">
                    </div>

                    <div class="container align-vertical">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
                                <h1 class="text-white">Login to continue</h1>
                                <div class="photo-form-wrapper clearfix">
                                    <form action="logincontroller" method="post">
                                        <input class="form-email" type="text" placeholder="Email Address" name="email">
                                        <input class="form-password" type="text" placeholder="name" name="name">
                                        <%
                                            if(request.getAttribute("error") != null){
                                             out.println("<input class=\"login-btn text-center btn-filled\" disabled=\"true\" editable=\"false\" type=\"text\" value=\""+request.getAttribute("error")+"\">");
                                            }
                                            %>
                                        <input class="login-btn btn-filled" type="submit" value="Login">
                                    </form>
                                </div><!--end of photo form wrapper-->

                            </div>
                        </div><!--end of row-->
                    </div><!--end of container-->
                </section>
            </div>
            <div class="footer-container"></div>

            <script src="js/jquery.min.js"></script>
            <script src="js/jquery.plugin.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/jquery.flexslider-min.js"></script>
            <script src="js/smooth-scroll.min.js"></script>
            <script src="js/skrollr.min.js"></script>
            <script src="js/spectragram.min.js"></script>
            <script src="js/scrollReveal.min.js"></script>
            <script src="js/isotope.min.js"></script>
            <script src="js/twitterFetcher_v10_min.js"></script>
            <script src="js/lightbox.min.js"></script>
            <script src="js/jquery.countdown.min.js"></script>
            <script src="js/scripts.js"></script>

        </body>

    </html>

