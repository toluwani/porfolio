

<%@page import="com.servlet.logincontroller"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html class="no-js"> 


    <head>
        <meta charset="utf-8">

        <title>Portfolio| Manager</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/theme-rocklobster.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="tolu.css" rel="stylesheet" type="text/css" media="all"/>

        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>


        <div class="nav-container">
            <nav class="top-bar overlay-bar">
                <div class="container">

                    <!--end of row-->


                    <div class="row nav-menu">
                        <div class="col-sm-3 col-md-2 columns">

                        </div>

                        <div class="col-sm-9 col-md-10 columns">
                            <ul class="menu">
                                
                                <% if(logincontroller.isLoggedIn){
                                    out.println("<li class=\"\"><a href=\"#\" id=\"myBtn\"> Upload files</a></li>");
                                }%>
                                
                                <li class=""><a href="https://www.upwork.com/i/how-it-works/client/"> find jobs</a>

                                </li>
                                
                                <% if(!logincontroller.isLoggedIn){
                                    out.println("<li class=\"\"><a href=\"login.jsp\"> Login</a></li>");
                                }%>
                                
                                <% if(logincontroller.isLoggedIn){
                                    out.println("<li><a href=\"#\" id=\"myBtn\"> Welcome "+logincontroller.name+"</a></li>");
                                }%>
                                
                                <% if(logincontroller.isLoggedIn){
                                    out.println("<li ><a href=\"login.jsp?x=x\" > Logout</a></li>");
                                }%>
                            </ul>


                            <!-- The Modal -->
                            <div id="myModal" class="modal">

                                <!-- Modal content -->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <span class="close">×</span>
                                        <h2>upload your photos and cv </h2>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <br class="row">
                                            <div class="panel panel-pink ">
                                                <div class="panel-heading col-sm-8 alert col-sm-offset-2">
                                                    <h3 class="panel-title" align="center"> Upload Stuff </h3>
                                                </div>

                                            </div>
                                            <div class="alert alert-warning col-sm-10 col-sm-offset-1">
                                                <form method="post" enctype="multipart/form-data" action="filehandeler" class="panel panel-info panel-body text-center alert-warning">
                                                    <div class="fallback">
                                                        <input name="id" value="<%= (session.getAttribute("user_id") == null) ? 1 : session.getAttribute("user_id") %>" type="hidden"/>
                                                        <input name="file" type="file" class="modalButton" />
                                                        <input type="submit" class="btn-grey" value="upload"/>
                                                        
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>

                        </div>







                        <ul class="social-icons text-right">
                            <li>
                                <a href="#">
                                    <i class="icon social_twitter"></i>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="icon social_facebook"></i>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="icon social_instagram"></i>
                                </a>
                            </li>
                            
                            
                        </ul>
                    </div>
                </div><!--end of row-->

                <div class="mobile-toggle">
                    <i class="icon icon_menu"></i>
                </div>

        </div><!--end of container-->
    </nav>



</div>

<div class="main-container">
    <header class="signup">
        <div class="background-image-holder parallax-background">
            <img class="background-image" alt="Background Image" src="img/hero5.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
                    <h1 class="text-white">Welcome to our amazing new service <br>sign up now and  upload your cv</h1>
                </div>
            </div><!--end of row-->

            <div class="row text-center">

                <div class="col-sm-12 text-center">
                    <div class="photo-form-wrapper clearfix">
                        <div class="row">
                            <form action="controller" method="POST">
                                <div class="col-md-3 col-sm-4">
                                    <input class="form-email" type="text" placeholder="name" name="name">
                                </div>

                                <div class="col-md-3 col-sm-4">
                                    <input class="form-password" type="text" placeholder="email" name="email">
                                </div>

                                <div class="col-md-3 col-sm-4">
                                    <input class="form-password2" type="text" placeholder="D.O.B" name="dob">
                                </div>

                                <div class="col-md-3 col-sm-4">
                                    <input class="form-password2" type="text" placeholder="State" name="state">
                                </div>

                                <div class="col-md-3 col-sm-4">
                                    <input class="form-password2" type="text" placeholder="L.G.A" name="lga">
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <input class="form-password2" type="text" placeholder="Marital Status" name="status">
                                </div>

                                <div class="col-md-3 col-sm-4">
                                    <input class="form-password2" type="text" placeholder="next of kin"  name="kin">

                                </div>

                                <div class="col-md-3 col-sm-4">
                                    <input class="form-email" type="text" placeholder="location" name="location">
                                </div>


                                <button type="submit" class="btn btn-success"> submit</button>
                            </form>
                        </div><!--end of row-->

                    </div><!--end of photo form wrapper-->


                </div>

            </div><!--end of row-->

        </div><!--end of container-->	
    </header>

    <section class="feature-divider">

        <div class="background-image-holder" data-scroll-reveal="wait 0.2s then enter 200px from bottom over 0.3s">
            <img class="background-image" alt="Background Image" src="img/grey-bg.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <h1>Our latest product has all the fancy features you could want</h1>
                    <p class="lead">
                        Be sure to upload your photo and resume and expect to get a new job as soon as possible

                    </p>
                    <a class="store-link" href="#"><img alt="Buy On App Store" src="img/app-store.png"></a>
                    <a class="store-link" href="#"><img alt="Buy On App Store" src="img/google-play.png"></a>
                </div>

                <div class="col-sm-7" data-scroll-reveal="enter from bottom and move 100px">
                    <img alt="App Screenshot" src="img/app2.png">
                </div>
            </div><!--end of row-->
        </div>
    </section>




</div>

<div class="footer-container">

    <footer class="social bg-secondary-1">

        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="text-white">Get In Touch</h1>
                    <a href="#" class="text-white"><strong> doctorflow74@gmail.com</strong></a><br>
                    <ul class="social-icons">
                        <li>
                            <a href="#">
                                <i class="icon social_twitter"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="icon social_facebook"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="icon social_instagram"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="icon social_dribbble"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="icon social_tumblr"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="icon social_pinterest"></i>
                            </a>
                        </li>
                    </ul><br>
                    <span class="sub">©  Aptech semester 4 Project <a href="#"> Java Web</a> - All Rights Reserved</span>
                </div>
            </div><!--end of row-->
        </div><!--end of container-->

    </footer>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/jquery.plugin.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/smooth-scroll.min.js"></script>
<script src="js/skrollr.min.js"></script>
<script src="js/spectragram.min.js"></script>
<script src="js/scrollReveal.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/twitterFetcher_v10_min.js"></script>
<script src="js/lightbox.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/scripts.js"></script>
<script src="tolu.js"></script>

</body>

</html>

