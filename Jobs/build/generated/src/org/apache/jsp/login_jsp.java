package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.servlet.logincontroller;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
 if(request.getParameter("x") != null){
    logincontroller.isLoggedIn = false;
    logincontroller.name = "";
}
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("\n");
      out.write("    <!DOCTYPE html>\n");
      out.write("    <!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->\n");
      out.write("    <!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->\n");
      out.write("    <!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->\n");
      out.write("    <!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->\n");
      out.write("\n");
      out.write("        <!-- Mirrored from pivot.mediumra.re/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Nov 2017 05:41:52 GMT -->\n");
      out.write("        <head>\n");
      out.write("            <meta charset=\"utf-8\">\n");
      out.write("\n");
      out.write("            <title>Pivot | Login</title>\n");
      out.write("            <meta name=\"description\" content=\"\">\n");
      out.write("            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("            <link href=\"css/flexslider.min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>\n");
      out.write("            <link href=\"css/line-icons.min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>\n");
      out.write("            <link href=\"css/elegant-icons.min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>\n");
      out.write("            <link href=\"css/lightbox.min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>\n");
      out.write("            <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>\n");
      out.write("            <link href=\"css/theme.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>\n");
      out.write("            <!--[if gte IE 9]>\n");
      out.write("                    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/ie9.css\" />\n");
      out.write("                    <![endif]-->\n");
      out.write("            <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>\n");
      out.write("            <script src=\"js/modernizr-2.6.2-respond-1.1.0.min.js\"></script>\n");
      out.write("        </head>\n");
      out.write("        <body>\n");
      out.write("            <div class=\"loader hidden\">\n");
      out.write("                <div class=\"spinner\">\n");
      out.write("                    <div class=\"double-bounce1\"></div>\n");
      out.write("                    <div class=\"double-bounce2\"></div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"nav-container\">\n");
      out.write("                <nav class=\"top-bar overlay-bar\">\n");
      out.write("                    <div class=\"container\">\n");
      out.write("\n");
      out.write("\n");
      out.write("                        <div class=\"row nav-menu\">\n");
      out.write("                            <div class=\"col-sm-3 col-md-2 columns\">\n");
      out.write("                                <a href=\"index.html\">\n");
      out.write("\n");
      out.write("                                </a>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"col-sm-9 col-md-10 columns\">\n");
      out.write("                                <ul class=\"menu\">\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                                    <li class=\"\"><a href=\"index.jsp\">home</a>\n");
      out.write("\n");
      out.write("                                    </li>\n");
      out.write("                                    <li class=\"\"><a href=\"https://www.upwork.com/i/how-it-works/client/\">findjobs</a>\n");
      out.write("\n");
      out.write("                                    </li>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                                </ul>\n");
      out.write("\n");
      out.write("                                <ul class=\"social-icons text-right\">\n");
      out.write("                                    <li>\n");
      out.write("                                        <a href=\"#\">\n");
      out.write("                                            <i class=\"icon social_twitter\"></i>\n");
      out.write("                                        </a>\n");
      out.write("                                    </li>\n");
      out.write("\n");
      out.write("                                    <li>\n");
      out.write("                                        <a href=\"#\">\n");
      out.write("                                            <i class=\"icon social_facebook\"></i>\n");
      out.write("                                        </a>\n");
      out.write("                                    </li>\n");
      out.write("\n");
      out.write("                                    <li>\n");
      out.write("                                        <a href=\"#\">\n");
      out.write("                                            <i class=\"icon social_instagram\"></i>\n");
      out.write("                                        </a>\n");
      out.write("                                    </li>\n");
      out.write("                                </ul>\n");
      out.write("                            </div>\n");
      out.write("                        </div><!--end of row-->\n");
      out.write("\n");
      out.write("                        <div class=\"mobile-toggle\">\n");
      out.write("                            <i class=\"icon icon_menu\"></i>\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                    </div><!--end of container-->\n");
      out.write("                </nav>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"main-container\">\n");
      out.write("                <section class=\"no-pad login-page fullscreen-element\">\n");
      out.write("\n");
      out.write("                    <div class=\"background-image-holder\">\n");
      out.write("                        <img class=\"background-image\" alt=\"Poster Image For Mobiles\" src=\"img/hero6.jpg\">\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"container align-vertical\">\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center\">\n");
      out.write("                                <h1 class=\"text-white\">Login to continue</h1>\n");
      out.write("                                <div class=\"photo-form-wrapper clearfix\">\n");
      out.write("                                    <form action=\"logincontroller\" method=\"post\">\n");
      out.write("                                        <input class=\"form-email\" type=\"text\" placeholder=\"Email Address\" name=\"email\">\n");
      out.write("                                        <input class=\"form-password\" type=\"text\" placeholder=\"name\" name=\"name\">\n");
      out.write("                                        ");

                                            if(request.getAttribute("error") != null){
                                             out.println("<input class=\"login-btn text-center btn-filled\" disabled=\"true\" editable=\"false\" type=\"text\" value=\""+request.getAttribute("error")+"\">");
                                            }
                                            
      out.write("\n");
      out.write("                                        <input class=\"login-btn btn-filled\" type=\"submit\" value=\"Login\">\n");
      out.write("                                    </form>\n");
      out.write("                                </div><!--end of photo form wrapper-->\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                        </div><!--end of row-->\n");
      out.write("                    </div><!--end of container-->\n");
      out.write("                </section>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"footer-container\"></div>\n");
      out.write("\n");
      out.write("            <script src=\"js/jquery.min.js\"></script>\n");
      out.write("            <script src=\"js/jquery.plugin.min.js\"></script>\n");
      out.write("            <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("            <script src=\"js/jquery.flexslider-min.js\"></script>\n");
      out.write("            <script src=\"js/smooth-scroll.min.js\"></script>\n");
      out.write("            <script src=\"js/skrollr.min.js\"></script>\n");
      out.write("            <script src=\"js/spectragram.min.js\"></script>\n");
      out.write("            <script src=\"js/scrollReveal.min.js\"></script>\n");
      out.write("            <script src=\"js/isotope.min.js\"></script>\n");
      out.write("            <script src=\"js/twitterFetcher_v10_min.js\"></script>\n");
      out.write("            <script src=\"js/lightbox.min.js\"></script>\n");
      out.write("            <script src=\"js/jquery.countdown.min.js\"></script>\n");
      out.write("            <script src=\"js/scripts.js\"></script>\n");
      out.write("\n");
      out.write("        </body>\n");
      out.write("\n");
      out.write("        <!-- Mirrored from pivot.mediumra.re/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Nov 2017 05:41:54 GMT -->\n");
      out.write("    </html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
