-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2017 at 01:31 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cv`
--

-- --------------------------------------------------------

--
-- Table structure for table `personal_details`
--

CREATE TABLE `personal_details` (
  `name` text NOT NULL,
  `email` varchar(192) NOT NULL,
  `dob` varchar(166) NOT NULL,
  `state` varchar(1281) NOT NULL,
  `lga` varchar(128) DEFAULT NULL,
  `status` varchar(128) DEFAULT NULL,
  `kin` varchar(128) NOT NULL,
  `location` varchar(123) NOT NULL,
  `id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personal_details`
--

INSERT INTO `personal_details` (`name`, `email`, `dob`, `state`, `lga`, `status`, `kin`, `location`, `id`) VALUES
(' tolu', 'doctorflow74@gmail.com', 'dec', 'ghgh', 'kogi', '', 'dfdfddff', '', 1),
('xxx', 'xx', 'xx', 'xxx', 'xx', 'xx', 'xxx', '', 2),
('dami', 'doctorflow744@gmail.com22', 'dec', 'lol', 'hjhj', 'single', 'ex', '', 3),
('dami', 'doctorflow744@gmail.com22', 'dec', 'lol', 'hjhj', 'single', 'ex', '', 4),
('dami', 'doctorflow744@gmail.com22', 'dec', 'lol', 'hjhj', 'single', 'ex', '', 5),
('dami', 'doctorflow744@gmail.com22', 'dec', 'lol', 'hjhj', 'single', 'ex', '', 6),
('dami', 'doctorflow744@gmail.com22', 'dec', 'lol', 'hjhj', 'single', 'ex', '', 7),
('dami', 'doctorflow744@gmail.com22', 'dec', 'lol', 'hjhj', 'single', 'ex', '', 8),
('ccc', 'cccc', 'ccc', 'ccc', 'ccc', 'ccc', 'ccc', '', 9),
('dami', 'doctorflow744@gmail.com22', 'dec', 'lol', 'hjhj', 'single', 'ex', '', 10),
('temi', 'doctorflow744@gmail.com22', 'rrrr', 'ghg', 'fgh', 'gghgg', 'tyu', '', 11),
('ddd', 'ddddd@sss', 'dddd', 'koji', 'ddddff', 'ffff', 'fff', 'ffff', 12),
('dd', 'ddd', 'dddd', 'dd', 'dd', 'dd', 'dd', 'dd', 15),
('ffff', 'fffff@VVVV', 'CCCC', 'VVVV', 'VVVBJ', 'FGJKK', 'DDDDD', 'HJKKK', 16);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `personal_details`
--
ALTER TABLE `personal_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `personal_details`
--
ALTER TABLE `personal_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
